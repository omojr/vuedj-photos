from rest_framework import serializers
from .models import Photo


class PhotoListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Photo
        fields = ('uid', 'owner', 'owner_username', 'image', 'thumbnail_url', 'title', 'description', 'folder',
                  'uploaded_readable',)
