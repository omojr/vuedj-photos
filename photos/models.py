from django.db import models
from django.contrib.auth.models import User

from easy_thumbnails.files import get_thumbnailer

from .common import get_uploaded_readable, generate_photo_path, get_unique_id


class Folder(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    parent = models.ForeignKey('self', related_name='children', on_delete=models.CASCADE, null=True)


class Photo(models.Model):
    uid = models.CharField(default=get_unique_id, max_length=15)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    image = models.ImageField(upload_to=generate_photo_path)
    folder = models.ForeignKey(Folder, related_name='photos', null=True, blank=True, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, blank=True)
    description = models.TextField(max_length=500, blank=True)
    uploaded = models.DateTimeField(auto_now_add=True)
    public = models.BooleanField(default=False)

    @property
    def uploaded_readable(self):
        return get_uploaded_readable(self.uploaded)

    @property
    def owner_username(self):
        return self.owner.username

    @property
    def thumbnail_url(self):
        return get_thumbnailer(self.image)['photo_list'].url


class Comment(models.Model):
    text = models.TextField(max_length=200, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='comments')
    photo = models.ForeignKey(Photo, on_delete=models.CASCADE, related_name='comments')
