from django.db.models.signals import post_save
from django.dispatch import receiver
from easy_thumbnails.files import get_thumbnailer

from .models import Photo


@receiver(post_save, sender=Photo)
def generate_thumbnail(sender, instance, created, **kwargs):
    if created:
        get_thumbnailer(instance.image)['photo_list'].url
