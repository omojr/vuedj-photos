from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from .views import PhotoAPIView, PhotoListAPIView, PhotoPublicListAPIView


urlpatterns = format_suffix_patterns([
    path('', PhotoListAPIView.as_view()),
    path('feed/', PhotoPublicListAPIView.as_view()),
    path('<str:uid>/', PhotoAPIView.as_view()),
])
