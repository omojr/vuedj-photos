# Generated by Django 2.0.1 on 2018-02-27 15:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('photos', '0004_photo_uid'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='public',
            field=models.BooleanField(default=False),
        ),
    ]
