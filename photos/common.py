import base64
import os

from django.template.defaultfilters import date as format_date
from django.utils import timezone


def get_uploaded_readable(d):
    if not d:
        return 'No info on upload date'
    if format_date(d, 'd_m_Y') == timezone.now().strftime('%d_%m_%Y'):
        return "today"
    elif format_date(d, 'm_Y') == timezone.now().strftime('%m_%Y') \
            and d.day + 1 == timezone.now().day:
        return "yesterday"
    elif d.year == timezone.now().year:
        return format_date(d, 'D, d b')
    else:
        return format_date(d, 'D, d b Y')


def get_unique_id():
    return base64.urlsafe_b64encode(os.urandom(12)).decode()


def generate_photo_path(instance, filename):
    file_extension = filename.split('.')[-1]
    name = get_unique_id()
    return f'users/{instance.owner.id}/photos/{name}.{file_extension}'
