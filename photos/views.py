from datetime import datetime
from time import time
import json
from time import sleep

from django.http import Http404
from rest_framework import status, permissions
from rest_framework.response import Response
from rest_framework_simplejwt import authentication
from rest_framework.views import APIView

from .models import Photo
from .serializers import PhotoListSerializer


class PhotoListAPIView(APIView):
    authentication_classes = (authentication.JWTAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        print(request.data)
        data = []
        for image in request.data.getlist('image'):
            data.append({"owner": request.user.pk, "folder": None, "image": image})
        print(data)
        serializer = PhotoListSerializer(data=data, many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status.HTTP_201_CREATED)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

    def get(self, request):
        start = int(request.GET.get('start', 0))
        before = request.GET.get('before', '')
        before_date = datetime.strptime(before, "%a, %d %b %Y %H:%M:%S %Z")
        photos = Photo.objects.filter(owner=request.user, uploaded__lte=before_date).order_by('-uploaded')[start:start+20]
        serializer = PhotoListSerializer(photos, many=True)
        sleep(1)
        return Response(data=serializer.data)


class PhotoPublicListAPIView(APIView):

    def get(self, request):
        start = int(request.GET.get('start', 0))
        before = request.GET.get('before', str(int(time())))
        before_date = datetime.fromtimestamp(before)
        photos = Photo.objects.filter(public=True, uploaded__lte=before_date).order_by('-uploaded')[start:start+20]
        serializer = PhotoListSerializer(photos, many=True)
        sleep(1)
        return Response(data=serializer.data)


class PhotoAPIView(APIView):
    authentication_classes = (authentication.JWTAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self, uid):
        try:
            return Photo.objects.get(uid=uid)
        except Photo.DoesNotExist:
            raise Http404

    def get(self, request, uid):
        photo = self.get_object(uid)
        serializer = PhotoListSerializer(photo)
        print(serializer.data)
        return Response(serializer.data)

    def put(self, request, uid):
        new_data = json.loads(request.body.decode())
        photo = self.get_object(uid)
        serializer = PhotoListSerializer(photo, data=new_data, partial=True)
        if serializer.is_valid():
            serializer.save()
        return Response(serializer.data, status.HTTP_200_OK)

    def delete(self, request, uid):
        photo = self.get_object(uid)
        if photo.owner == self.request.user:
            photo.delete()
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)
        return Response(status=status.HTTP_204_NO_CONTENT)
