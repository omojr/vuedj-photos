from django.db import models
from django.contrib.auth.models import User


def generate_upload_path(instance, filename):
    file_extension = filename.split('.')[-1]
    return f'users/{instance.user.username}/profile/avatar.{file_extension}'


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to=generate_upload_path, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    memory_total = models.PositiveIntegerField(default=300)
    memory_used = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ('created',)

    def __str__(self):
        return f'Profile {self.user.username}'

    def save(self, *args, **kwargs):
        try:
            this = Profile.objects.get(id=self.id)
            this.avatar.delete()
        except:
            pass
        super(Profile, self).save(*args, **kwargs)
