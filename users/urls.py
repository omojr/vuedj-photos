from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from .views import UserList, UserDetail


urlpatterns = format_suffix_patterns([
    path('', UserList.as_view()),
    path('<int:pk>/', UserDetail.as_view()),
])
