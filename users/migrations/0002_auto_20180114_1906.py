# Generated by Django 2.0.1 on 2018-01-14 19:06

from django.db import migrations, models
import users.models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='profile',
            options={'ordering': ('created',)},
        ),
        migrations.RemoveField(
            model_name='profile',
            name='photo',
        ),
        migrations.AddField(
            model_name='profile',
            name='avatar',
            field=models.ImageField(blank=True, upload_to=users.models.generate_upload_path),
        ),
        migrations.AddField(
            model_name='profile',
            name='memory_total',
            field=models.PositiveIntegerField(default=300),
        ),
        migrations.AddField(
            model_name='profile',
            name='memory_used',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
